import React, {useEffect} from 'react';
import {Text, Image, View, StyleSheet} from 'react-native';
import Google from '../../assets/Google.png';

const Splash = ({navigation}) => {
    useEffect(() => {
        setTimeout(() => {
            navigation.replace('Welcome');
        }, 2500);
    });
    return (
        <View style={styles.wrapper}>
            <Image source={Google} style={styles.logo}/>
        </View>
    );
};

export default Splash;

const styles = StyleSheet.create({
    wrapper: {
        flex : 1,
        backgroundColor : 'white',
        alignItems : 'center',
        justifyContent : 'center',
    },
    logo : {
        margin : 10,
        resizeMode : 'contain',
        height : '30%',
    },
    welcomeText : {
        fontSize : 24,
        fontWeight : 'bold',
        color : 'green',
        paddingBottom : 20,
    },
});