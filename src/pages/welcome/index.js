import React, {useEffect} from 'react';
import {TextInput, View, TouchableOpacity, Text, StyleSheet, Image} from 'react-native';
import Logo from '../../assets/Bareng_Saya.png';
import Google from '../../assets/Google_Text.png';

const WelcomeAuth = () => {
    return (
        <View style={styles.wrapper}>
            <Image source={Google} style={styles.googleIcon}/>
            <TextInput 
                style={styles.form}
                placeholder = "Search Here"
            />
        </View>
    );
};

export default WelcomeAuth;

const styles = StyleSheet.create({
    wrapper: {
        display : 'flex',
        flex : 1,
        flexDirection : 'row',
        backgroundColor : 'white',
        alignItems : 'center',
        justifyContent : 'center',
        position : 'relative',
    },
    form : {
        height : 40,
        width : 350,
        backgroundColor : 'white',
        textAlign : 'left',
        borderRadius : 100,
        borderColor : 'black',
        borderWidth : 1,
        margin : 3,
        paddingLeft : 20,
    },
    googleIcon : {
        resizeMode : 'contain',
        height : '20%',
        position : 'absolute',
        top : 150,
        justifyContent : 'center',
    },
});